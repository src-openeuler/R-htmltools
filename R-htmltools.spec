# Bootstrap
%bcond_without bootstrap

%global packname htmltools
%global packver  0.5.1.1
%global rlibdir  %{_libdir}/R/library

Name:             R-%{packname}
Version:          0.5.1.1
Release:          1
Summary:          Tools for HTML

License:          GPLv2+
URL:              https://CRAN.R-project.org/package=%{packname}
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{packver}.tar.gz

BuildRequires:    R-devel
BuildRequires:    tex(latex)
BuildRequires:    R-utils
BuildRequires:    R-digest
BuildRequires:    R-grDevices
BuildRequires:    R-base64enc
BuildRequires:    R-rlang
%if %{without bootstrap}
BuildRequires:    R-markdown
BuildRequires:    R-testthat
BuildRequires:    R-withr
BuildRequires:    R-Cairo
BuildRequires:    R-ragg
BuildRequires:    R-shiny
%endif

%description
Tools for HTML generation and output.


%prep
%setup -q -c -n %{packname}


%build


%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css


%check
%if %{without bootstrap}
export LANG=C.UTF-8
%{_bindir}/R CMD check %{packname}
%endif


%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%{rlibdir}/%{packname}/DESCRIPTION
%doc %{rlibdir}/%{packname}/NEWS.md
%{rlibdir}/%{packname}/INDEX
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/help
%dir %{rlibdir}/%{packname}/libs
%{rlibdir}/%{packname}/libs/%{packname}.so


%changelog
* Thu Jun 16 2022 misaka00251 <misaka00251@misakanet.cn> - 0.5.1.1-1
- Init package (Thanks to fedora team)
